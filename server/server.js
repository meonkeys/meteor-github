const github = new GitHubApi({
  version: '3.0.0',
  debug: true,
  protocol: 'https',
});

const wrappedFollowers = Meteor.wrapAsync(github.user.getFollowers, github.user);

Meteor.methods({
  'getOneFollower': function(username) {
    const rv = wrappedFollowers({user: username, page: 1, per_page: 1});
    return rv[0];
  }
});
