Package.describe({
  name: 'meonkeys:github',
  version: '0.0.1',
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.0.2');
  api.use('ecmascript');
  api.addFiles('github.js', 'server');
  api.export('GitHubApi');
});

Npm.depends({
  github: '0.2.4'
});
