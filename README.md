# Meteor NPM package example - github API

An example Meteor app which wraps and uses the github npm package (server-side).

## Update: npm :heart: Meteor

Note: [Meteor 1.3 will make all of this unnecessary](https://medium.com/@borellvi/meteor-meets-npm-a5cc48d90abe).

## Description

Written for <https://www.reddit.com/r/Meteor/comments/3ncsc7/how_do_you_use_npm_packages_within_meteor/>.

Created via:

1. Initialize Meteor app: `meteor create meteor-github`. The following steps were done in that dir unless otherwise specified.
1. Create a local package (inside your Meteor app) to wrap [github npm package](https://www.npmjs.com/package/github): `mkdir packages && cd packages && meteor create --package meonkeys:github`. `meonkeys` can be whatever you prefer. Your username on meteor.com is a fine choice here (especially if you might someday publish the package on atmosphere).
1. Add `Npm.require`, `api.addFiles` and `api.export` lines to `packages/github/package.js`.
1. Add `Npm.depends` block to `packages/github/github.js`.
1. `meteor add meonkeys:github`
