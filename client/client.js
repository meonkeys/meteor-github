const resultVar = new ReactiveVar('click the button');

Template.hello.helpers({
  result: function() {
    return resultVar.get();
  }
});

Template.hello.events({
  'click button': function () {
    Meteor.call('getOneFollower', 'meonkeys', function(error, result) {
      if (error) {
        resultVar.set(JSON.stringify(error, null, 2));
      } else {
        resultVar.set(JSON.stringify(result, null, 2));
      }
    });
  }
});
